﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;
using System.Media;
using System.Threading;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading.Tasks;
using Npgsql;
using System.Net;
using Oracle.ManagedDataAccess.Client;
using System.Management;
using WinFormCharpWebCam;

namespace door
{
    public partial class Form1 : Form
    {
        psuRegfile.StaffExtend obj = new psuRegfile.StaffExtend();

        public delegate void BarcodeReceivedEventHandler(object sender, BarcodeReceivedEventArgs e);
        public class BarcodeReceivedEventArgs : EventArgs
        {

            private string _barcode;

            public BarcodeReceivedEventArgs(string Barcode)
            {

                _barcode = Barcode;

            }

            public string Barcode { get { return _barcode; } }

        }
        public event BarcodeReceivedEventHandler BarcodeReceived;

        const char ScannerEndCharacter = '\r';

        private StringBuilder scannedCharacters;

        List<string> dataItems = new List<string>();

        //ตัวนับถอยหลังเวลา
        int timeCheck = 0;

        //ตั้งค่าเวลาเป็นเวลาปัจจุบัน
        System.Windows.Forms.Timer tmr = null;

        //ตัวแปร hardware
        int q;
        int a;
        delegate void SetTextCallback();

        //ตัวแปรของกล้อง
        WebCam webcam;


        bool isCameraRunning = false;

        //ตัวแปรของ NFC
        int retCode;
        int hCard;
        int hContext;
        int Protocol;
        public bool connActive = false;
        string readername = "ACS ACR122 0";      // change depending on reader
        public byte[] SendBuff = new byte[263];
        public byte[] RecvBuff = new byte[263];
        public int SendLen, RecvLen, nBytesRet, reqType, Aprotocol, dwProtocol, cbPciLength;
        public Card.SCARD_READERSTATE RdrState;
        public Card.SCARD_IO_REQUEST pioSendRequest;
        private BackgroundWorker _worker;
        private Card.SCARD_READERSTATE[] states;

        //ตัวแปรเช็คสถานะ
        bool statuscard = false;
        bool statusbarcode = false;

        //State ของ Card
        internal enum SmartcardState
        {
            None = 0,
            Inserted = 1,
            Ejected = 2
        }

        //database
        string connstring = "Server=192.168.26.19; Port=5432; User Id=ticket; Password=abc123; Database=ticket;";
        string connetionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\software-and-hardware\\database\\REPORT_CIRMEMBER.mdb;";

        //การแสดงผล
        public Form1()
        {
            InitializeComponent();
            StartTimer();

            SelectDevice();       //For RFID
            establishContext();  //For  RFID

            Watch();
            scannedCharacters = new StringBuilder();

            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(Form1_KeyPress); //เพื่อตรวจสอบการอ่านของเครื่องอ่าน 


            this.BarcodeReceived += new BarcodeReceivedEventHandler(Form1_BarcodeReceived); //หากมีการอ่านของบาร์โค้ดจะเรียกใช้ฟังก์ชันในการอ่าน

            //รุปภาพเริ่มต้น
            pictureBox2.Image = Image.FromFile(@"\software-and-hardware\picture\cam2.gif");
            pictureBox3.Image = Image.FromFile(@"\software-and-hardware\picture\cam2.gif");
            pictureBox1.Image = Image.FromFile(@"\software-and-hardware\picture\cam.gif");

        }
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)//ฟังก์ชันที่ตรวจสอบการอ่านของเครื่องอ่าน
        {
            if (e.KeyChar == 13)    //การแสกนจะตัดที่การ Enter (keychar==13 คือ Enter)
            {
                e.Handled = true;
                OnBarcodeRecieved(new BarcodeReceivedEventArgs(scannedCharacters.ToString()));
                scannedCharacters.Remove(0, scannedCharacters.Length);
            }

            else
            {
                scannedCharacters.Append(e.KeyChar);
                e.Handled = false;
            }
        }
        protected virtual void OnBarcodeRecieved(BarcodeReceivedEventArgs e)//ฟังก์ชันที่ใช้จัดการหลังตรวจสอบว่าเครื่องอ่านทำการอ่านบาร์โค้ดอ่านได้
        {
            if (BarcodeReceived != null)
                BarcodeReceived(this, e); //ส่งค่ากลับไปที่ Form1 ก่อน Form1 จะเรียกใช้ฟัง์ชัน Form1_BarcodeReceived

        }
        void Form1_BarcodeReceived(object sender, BarcodeReceivedEventArgs e) //ฟังก์ชันอ่านบาร์โค้ด
        {
            string barcodeIdentify = e.Barcode;
            string IdentifyBC = barcodeIdentify.Substring(0, 2);
            //Console.WriteLine("***************" + IdentifyES);

            //นำค่าที่ได้จากการอ่านบาร์โค้ดไป query ใน database
            //MessageBox.Show("Barcode scanned: " + e.Barcode);
            if ((e.Barcode.Length == 10) && (IdentifyBC == "E$") && (serialPort1.RtsEnable == false))  //หากอ่านบาร์โค้ดได้ทั้งหมด 10 ตัวอักษร (ถ้าเพิ่มความยาวของบาร์โค้ด ต้องแก้เลข 10)
            {
                timeCheck = 0;
                serialPort1.RtsEnable = false;
                string str = null;
                string esString = null;
                string typeString = null;
                string idString = null;
                string keyString = null;
                string typeMember = null;
                str = e.Barcode;
                string barcodeId = str;
                Console.WriteLine("***************" + str);
                //ทำการ subString เพื่อแยก code ที่อ่านได้เป็นส่วนๆ เช่น id ประเภทของticket เป็นต้น
                esString = str.Substring(0, 2);
                typeString = str.Substring(2, 1);
                idString = str.Substring(3, 4);
                keyString = str.Substring(7, 3);
                UpdatingTxtAll("   รหัสประจำตัว       :   " + barcodeId);
                int typeUser = Int32.Parse(typeString);
                string pushData = "";
                //เช็คว่าเป็นบัตร เด็กหรือผู้ใหญ่
                if(typeUser == 1) 
                {
                    typeMember = "เด็ก";
                }
                else
                {
                    typeMember = "ผู้ใหญ่";
                } 
                try
                {
                    //เข้าฐานข้อมูล
                    
                    NpgsqlConnection connection = new NpgsqlConnection(connstring);
                    connection.Open();
                    NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM tkstamp WHERE time_stamp >=' " + DateTime.Now.ToString("yyyyMMdd") + "' AND user_type = '" + typeString + "'  AND id = '" + idString + "' AND key_secret = '" + keyString + "' ", connection);
                    NpgsqlDataReader dataRead = command.ExecuteReader();
                    if (dataRead.Read())
                    {
                        statusbarcode = true;
                        //แสดง ชื่อ และนามสกุล ลงในtextbox
                        UpdatingTxtAll2("   ประเภทบุคคล      :   " + typeMember.ToString());
                        UpdatingTxtAll3("   วันที่เข้าใช้งาน     :    " + DateTime.Now.ToString("dd/MM/yyyy"));
                        UpdatingTxtAll4("   เวลาที่เข้าใช้งาน    :    " + DateTime.Now.ToString("HH:mm:ss น."));
                        //logBarcode type เด็ก ผู้ใหญ่ ID Key
                        LoggerBarcode("UserType:" + typeString + "    ID: " + idString + "    Key: " + keyString);
                        Image imageCheck1 = null;
                            if(typeUser == 1)
                            {
                                imageCheck1 = Image.FromFile(@"\software-and-hardware\picture\kid.jpg");
                                pictureBox3.Image = imageCheck1;
                                PlaySound4();
                            }
                            else if(typeUser == 2)
                            {
                                imageCheck1 = Image.FromFile(@"\software-and-hardware\picture\adult.jpg");
                                pictureBox3.Image = imageCheck1;
                                PlaySound5();
                        }
                        //ถ่ายภาพ
                        useCapture();
                     
                        lightAsync();
                        pushData = "OK";
                    }
                    else //ไม่พบข้อมูลในกรณีไม่มีข้อมูลในฐานข้อมูล
                    {
                        statusbarcode = false;
                        PrintNoDataBarcode();
                    }
                    dataRead.Close();
                    command.Dispose();
                    connection.Close();
                }
                catch (Exception) //ทำงานเมื่อ เกิด Exception
                {
                    statusbarcode = false;
                    PrintNoDataBarcode();
                }
                if(pushData == "OK")
                {
                    QRCMD(barcodeId, typeUser.ToString());
                }
            }
            else if ((IdentifyBC == "TM") && (serialPort1.RtsEnable == false))
            {
                string strCode = e.Barcode;
                string alphabet = "QU2HAd0w3VCspWrokXmyNzYbIJKvLMhij85DOn1Sce49gP7BEuTfZ6FxqRlatG!#$%&()*+,-.:;=?@[]^_`{|}~";
                string permuted = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&()*+,-.:;=?@[]^_`{|}~";
                int i = 0;
                int indexWord = 0;
                string result = "";
                string word = strCode;
                Console.WriteLine("input id : " + word);
                try
                {
                    while (i < word.Length)
                    {
                        //ถ้า word ไม่อยู่ใน alphabet จะออกมาเป็นตัวเดิม
                        indexWord = alphabet.IndexOf(word[i]);
                        result = result + permuted[indexWord]; //expected is 28KTKScS_ZF
                        i++;
                    }
                }
                catch (Exception)
                {
                    result = word;
                }
                Console.WriteLine("decrypt id : " + result);

                string StdID = result.Substring(16, result.Length - 16);

                Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + StdID);
                string pushData = "";
                string firstname = "", lastname = "", titleid = "", member = "", faculty = "", department = "";

                OleDbConnection con = new OleDbConnection(connetionString);
                try
                {
                    con.Open();
                    //เชื่อต่อกับฐานข้อมูล
                    OleDbCommand cmd = new OleDbCommand("SELECT * FROM LOCAL_CIRMEMBER WHERE MEMBERCODE = '" + StdID + "' ", con);
                    OleDbDataReader dataReader = cmd.ExecuteReader();
                    if (dataReader.Read())
                    {
                        string dateNow = DateTime.Now.ToString();
                        string expireDate = "" + result.Substring(2, 2) + "/" + result.Substring(4, 2) + "/" + result.Substring(8, 2) + " " + "" + result.Substring(10, 2) + ":" + result.Substring(12, 2) + ":" + result.Substring(14, 2);
                        Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + expireDate);

                        if (DateTime.Parse(expireDate) < DateTime.Parse(dateNow))
                        {
                            statuscard = false;
                            UpdatingTxtAll("   รหัสประจำตัว    :   " + StdID);
                            UpdatingTxtAll2("   ชื่อ-สกุล            :   " + dataReader[1].ToString() + "   " + dataReader[2].ToString());
                            UpdatingTxtAll3("   วันที่เข้าใช้งาน  :   คิวอาร์โค้ดหมดอายุ ");
                            UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   คิวอาร์โค้ดหมดอายุ ");
                            Image imageCheck2 = null;
                            imageCheck2 = Image.FromFile(@"\software-and-hardware\picture\expiredqrcode.jpg");    //แสดงรูปภาพ Nodata
                            pictureBox3.Image = imageCheck2;
                            pictureBox2.Image = imageCheck2;
                            PlaySound8();
                        }
                        else
                        {
                            firstname = dataReader[1].ToString();
                            lastname = dataReader[2].ToString();
                            titleid = dataReader[3].ToString();
                            member = dataReader[4].ToString();
                            faculty = dataReader[5].ToString();
                            department = dataReader[6].ToString();
                            statuscard = true;
                            UpdatingTxtAll("   รหัสประจำตัว    :   " + StdID);
                            UpdatingTxtAll2("   ชื่อ-สกุล            :   " + dataReader[1].ToString() + "   " + dataReader[2].ToString());
                            UpdatingTxtAll3("   วันที่เข้าใช้งาน   :   " + DateTime.Now.ToString("dd/MM/yyyy"));
                            UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   " + DateTime.Now.ToString("HH:mm:ss น."));
                            //logCard รหัส ชื่อ สกุล วันเวลา
                            LoggerCard("จาก QRcode บนเว็บ " + "stdID: " + StdID + "   FirstName: " + dataReader[1].ToString() + "     SureName: " + dataReader[2].ToString());
                            Console.WriteLine("################" + StdID);

                            Picweb(StdID);

                            //ถ่ายภาพ
                            useCapture();
                            //เล่นเสียงยินดีย้อนรับค่ะ
                            PlaySound1();
                            //ไฟเขียวติด 4 วินาที
                            lightAsync();
                            pushData = "OK";
                        }
                    }
                    else //ไม่พบข้อมูลในกรณีไม่มีข้อมูลในฐานข้อมูล
                    {
                        UpdatingTxtAll("   รหัสประจำตัว    :   " + StdID);
                        statuscard = false;
                        PrintNoDataRfid();
                    }
                    //เมื่อ read แล้ว จะต้อง close ทุกครั้ง
                    dataReader.Close();
                    cmd.Dispose();
                    con.Close();
                }
                catch (Exception)
                {
                    statuscard = false;
                    UpdatingTxtAll("   รหัสประจำตัว    :   " + StdID);
                    PrintNoDataRfidfail();
                }
                if (pushData == "OK")
                {
                    BarcodeandRFIDCMD(StdID, firstname, lastname, titleid, member, faculty, department);
                }
            }
            else if (serialPort1.RtsEnable == false)
            {
                string StdID = barcodeIdentify;
                UpdatingTxtAll("   รหัสประจำตัว    :   " + StdID);
                string pushData = "";
                string firstname = "", lastname = "", titleid = "", member = "", faculty = "", department = "";
                
                OleDbConnection con = new OleDbConnection(connetionString);
                try
                {
                    con.Open();
                    //เชื่อต่อกับฐานข้อมูล
                    OleDbCommand cmd = new OleDbCommand("SELECT * FROM LOCAL_CIRMEMBER WHERE MEMBERCODE = '" + StdID + "' ", con);
                    OleDbDataReader dataReader = cmd.ExecuteReader();

                    //อ่านข้อมูลในฐานข้อมูล
                    if (dataReader.Read())
                    {
                        //แสดง ชื่อ และนามสกุล ลงในtextbox

                        string dateNow = DateTime.Now.ToString(); //วันปัจจุบัน
                        string expireDate = dataReader[8].ToString(); //วันหมดอายุในบัตร

                        //เช็ควันหมดอายุของบัตร
                        //ถ้าหมดอายุ
                        if (DateTime.Parse(expireDate) < DateTime.Parse(dateNow))
                        {
                            statuscard = false;
                            UpdatingTxtAll2("   ชื่อ-สกุล            :   " + dataReader[1].ToString() + "   " + dataReader[2].ToString());
                            UpdatingTxtAll3("   วันที่เข้าใช้งาน  :   บัตรหมดอายุ ");
                            UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   บัตรหมดอายุ ");
                            Image imageCheck2 = null;
                            imageCheck2 = Image.FromFile(@"\software-and-hardware\picture\expired.jpg");    //แสดงรูปภาพ Nodata
                            pictureBox3.Image = imageCheck2;
                            pictureBox2.Image = imageCheck2;
                            PlaySound3();
                        }
                        //ถ้าไม่หมดอายุ
                        else
                        {
                            firstname = dataReader[1].ToString();
                            lastname = dataReader[2].ToString();
                            titleid = dataReader[3].ToString();
                            member = dataReader[4].ToString();
                            faculty = dataReader[5].ToString();
                            department = dataReader[6].ToString();
                            statuscard = true;
                            UpdatingTxtAll2("   ชื่อ-สกุล            :   " + dataReader[1].ToString() + "   " + dataReader[2].ToString());
                            UpdatingTxtAll3("   วันที่เข้าใช้งาน   :   " + DateTime.Now.ToString("dd/MM/yyyy"));
                            UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   " + DateTime.Now.ToString("HH:mm:ss น."));
                            //logCard รหัส ชื่อ สกุล วันเวลา
                            LoggerCard("จาก Barcode บนบัตร " + "stdID: " + StdID + "   FirstName: " + dataReader[1].ToString() + "     SureName: " + dataReader[2].ToString());
                            Console.WriteLine("################" + StdID);
                            //ถ้ามีรหัส น.ศ ฐานข้อมูลจะทำการดึงรูปภาพจากฐานข้อมูลมาแสดง
                            /*
                            Image imageCheck1 = null;
                            imageCheck1 = Image.FromFile(@"\software-and-hardware\picture\" + StdID + ".jpg");
                             pictureBox3.Image = imageCheck1;
                             */

                            Picweb(StdID);

                            //ถ่ายภาพ
                            useCapture();
                            //เล่นเสียงยินดีย้อนรับค่ะ
                            PlaySound1();
                            //ไฟเขียวติด 4 วินาที
                            lightAsync();
                            pushData = "OK";
                        }

                    }
                    else //ไม่พบข้อมูลในกรณีไม่มีข้อมูลในฐานข้อมูล
                    {
                        statuscard = false;
                        PrintNoDataRfid();
                    }
                    //เมื่อ read แล้ว จะต้อง close ทุกครั้ง
                    dataReader.Close();
                    cmd.Dispose();
                    con.Close();
                }
                catch (Exception) //ทำงานเมื่อ เกิด Exception
                {
                    statuscard = false;
                    PrintNoDataRfidfail();
                }
                if (pushData == "OK")
                {
                    BarcodeandRFIDCMD(StdID, firstname, lastname, titleid, member, faculty, department);
                }

            }

        }

        
        private void Form1_Load(object sender, EventArgs e)
        {
            
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    object myObject = queryObj["Caption"];
                    if (myObject != null)
                    {
                        string myObjectString = myObject.ToString();
                        if (myObjectString == "HD Webcam C270")
                        {
                            webcam = new WebCam();
                            webcam.InitializeWebCam(ref pictureBox1);
                            webcam.Start();

                            isCameraRunning = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("---------------ERROR-----------------");
            }
            
            serialPort1.Open();
            serialPort1.PinChanged += new SerialPinChangedEventHandler(port_PinChanged);
            timer1.Enabled = true;
            timer2.Enabled = true;
        }   //เมื่อเปิด form จะทำ
        private void Form1_FormClosed(object sender, FormClosedEventArgs e) 
        {
            isCameraRunning = false;
            webcam.Stop();

            if (q != 0)
            {
                LoggerCount("จำนวนการเข้าใช้: " + q.ToString() + " ครั้ง");
            }
        }      //เมื่อปิด form จะทำ

        //ควยคุม hardware
        private void Timer1_Tick_1(object sender, EventArgs e)
        {
            a = listBox1.Items.Count;
            // MessageBox.Show(j.ToString(), "Timer1");
        }
        private async void Timer2_TickAsync(object sender, EventArgs e)
        {
            //MessageBox.Show(j.ToString(), "Timer2");
            if (a == listBox1.Items.Count && a != 0)
            {
                q++;
                string resetCountDown = "10";
                UpdatingtxtCountDown(resetCountDown.ToString());
                this.label10.Text = q.ToString();
                listBox1.Items.Clear();
                timer1.Enabled = false;
                timer2.Enabled = false;

                
                if (statuscard)
                {
                    serialPort1.DtrEnable = false; //ปิดเสียง
                    serialPort1.RtsEnable = false; //ไฟเขียวดับ
                    statuscard = false;
                }
                else if (statusbarcode)
                {
                    serialPort1.DtrEnable = false; //ปิดเสียง
                    serialPort1.RtsEnable = false; //ไฟเขียวดับ
                    statusbarcode = false;
                }
                else 
                {
                    PlaySound7();
                    serialPort1.DtrEnable = true; //เสียงร้อง
                    await Task.Delay(1000); // ดีเลย์แบบไม่ sleep
                    //Thread.Sleep(2000);   //ดีเลย์แบบ sleep
                    serialPort1.DtrEnable = false; //ปิดเสียง
                }
                    
                
            }
        }
        public void UpdatingtxtCountDown(string msg)
        {
            if (this.txtCountDown.InvokeRequired)
                this.txtCountDown.Invoke(new TextBoxDelegate(UpdatingtxtCountDown), new object[] { msg });

            else
                this.txtCountDown.Text = msg;
        }
        public void port_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // MessageBox.Show(j.ToString(), "pinchange");
            if (e.EventType == SerialPinChange.CtsChanged)
            {
                SetText();

            }
        }
        private void SetText()
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.

            if (this.label10.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d);

            }
            else
            {

                listBox1.Items.Add(SerialPinChange.CtsChanged);
                timer1.Enabled = true;
                timer2.Enabled = true;

            }

        }

        //วนลูปเช็คสถานะของ card ว่ามีบัตรอยู่หรือไม่
        private void WaitChangeStatus(object sender, DoWorkEventArgs e)
        {
            while (!e.Cancel)
            {
                int nErrCode = Card.SCardGetStatusChange(hContext, 1000, ref states[0], 1);
                //ตรวจว่ามีการเปลี่ยนสถานะล่าสุดหรือไม่
                if ((this.states[0].RdrEventState & 2) == 2)
                {
                    //ตรวจว่ามีอะไรเปลี่ยน
                    SmartcardState state = SmartcardState.None;
                    if ((this.states[0].RdrEventState & 32) == 32 && (this.states[0].RdrCurrState & 32) != 32)
                    {
                        //มี card อยู่
                        state = SmartcardState.Inserted;
                    }
                    else if ((this.states[0].RdrEventState & 16) == 16 && (this.states[0].RdrCurrState & 16) != 16)
                    {
                        //ไม่มี card อยู่
                        state = SmartcardState.Ejected;
                    }
                    if (state != SmartcardState.None && this.states[0].RdrCurrState != 0)
                    {
                        switch (state)
                        {
                            case SmartcardState.Inserted:   //เมื่อมี card
                                {
                                    //MessageBox.Show("Card inserted");
                                    if(serialPort1.RtsEnable == false)
                                    {
                                        read();
                                    }
                                    
                                    break;
                                }
                            case SmartcardState.Ejected:    //เมื่อไม่มี card
                                {
                                    //MessageBox.Show("Card ejected");
                                    Disconnect();
                                    break;
                                }
                            default:
                                {
                                    //MessageBox.Show("Some other state...");
                                    break;
                                }
                        }
                    }
                    //อัพเดตสถานะล่าสุดสำหรับการตรวจสอบครั้งถัดไป
                    this.states[0].RdrCurrState = this.states[0].RdrEventState;
                }
            }
        }
        //อ่านค่าของบัตร
        private void read()
        {
        
            //กำหนดตำแหน่งฐานข้อมูล
           
            bool connectActive = connectCard();

            if (connectActive)  //ตรวจสอบว่ามีการเชื่อมต่อหรือไม่
            {
                timeCheck = 0;
                serialPort1.RtsEnable = false;
                string StdID = verifyCard("1");      //เก็บรหัส น.ศ ที่อยู่ใน blockที่ 1 ของบัตรโดยนำไปตรวจสอบกับ key แล้วเก็บในตัวแปร StdID
                Console.WriteLine(">>>>>>>>>>" + StdID);
                string pushData = "";
                string firstname = "", lastname = "" , titleid = "", member = "" , faculty = "", department = "";
                string cardExpire = "";
                UpdatingTxtAll("   รหัสประจำตัว    :   " + StdID);   //แสดงรหัส น.ศ.
                //เชื่อมต่อฐานข้อมูล
                if (StdID != "FailAuthentication" && StdID != "Error") //ถ้าตรวจสอบ รหัส น.ศ กับ UID แล้วเป็บตามเงื่อนไข จะเข้า try แต่ถ้าไม่ จะเข้า catch
                {
                    OleDbConnection con = new OleDbConnection(connetionString);
                    try
                    {
                        con.Open();
                        //เชื่อต่อกับฐานข้อมูล
                        OleDbCommand cmd = new OleDbCommand("SELECT * FROM LOCAL_CIRMEMBER WHERE MEMBERCODE = '" + StdID + "' ", con);
                        OleDbDataReader dataReader = cmd.ExecuteReader();   

                        //อ่านข้อมูลในฐานข้อมูล
                        if (dataReader.Read())
                        {
                            //แสดง ชื่อ และนามสกุล ลงในtextbox

                            string dateNow = DateTime.Now.ToString(); //วันปัจจุบัน
                            string expireDate = dataReader[8].ToString(); //วันหมดอายุในบัตร

                            //เช็ควันหมดอายุของบัตร
                            //ถ้าหมดอายุ
                            if (DateTime.Parse(expireDate) < DateTime.Parse(dateNow))
                            {
                                cardExpire = "EXPIRED";
                            }
                            //ถ้าไม่หมดอายุ
                            else
                            {
                                firstname = dataReader[1].ToString();
                                lastname = dataReader[2].ToString();
                                titleid = dataReader[3].ToString();
                                member = dataReader[4].ToString();
                                faculty = dataReader[5].ToString();
                                department = dataReader[6].ToString();
                                
                                statuscard = true;
                                UpdatingTxtAll2("   ชื่อ-สกุล            :   " + dataReader[1].ToString() + "   " + dataReader[2].ToString());
                                UpdatingTxtAll3("   วันที่เข้าใช้งาน   :   " + DateTime.Now.ToString("dd/MM/yyyy"));
                                UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   " + DateTime.Now.ToString("HH:mm:ss น."));
                                //logCard รหัส ชื่อ สกุล วันเวลา
                                LoggerCard("stdID: " + StdID + "   FirstName: " + dataReader[1].ToString() + "     SureName: " + dataReader[2].ToString());
                                Console.WriteLine("################" + StdID);

                                //ถ้ามีรหัส น.ศ ฐานข้อมูลจะทำการดึงรูปภาพจากฐานข้อมูลมาแสดง
                                /*
                                Image imageCheck1 = null;
                                imageCheck1 = Image.FromFile(@"\software-and-hardware\picture\" + StdID + ".jpg");
                                 pictureBox3.Image = imageCheck1;
                                 */

                                Picweb(StdID);

                                //ถ่ายภาพ
                                useCapture();
                                //เล่นเสียงยินดีย้อนรับค่ะ
                                PlaySound1();
                                //ไฟเขียวติด 4 วินาที
                                lightAsync();
                                pushData = "OK";
                            }

                        }
                        else //ไม่พบข้อมูลในกรณีไม่มีข้อมูลในฐานข้อมูล
                        {
                            statuscard = false;
                            PrintNoDataRfid();
                        }
                        //เมื่อ read แล้ว จะต้อง close ทุกครั้ง
                        dataReader.Close();
                        cmd.Dispose();
                        con.Close();
                    }
                    catch (Exception) //ทำงานเมื่อ เกิด Exception
                    {
                        statuscard = false;
                        PrintNoDataRfidfail();
                    }
                    if (pushData == "OK")
                    {
                        BarcodeandRFIDCMD(StdID, firstname, lastname, titleid, member, faculty, department);
                    }
                    if (cardExpire == "EXPIRED")
                    {
                        oracleCheck(StdID);
                    }
                }
                else //ยินยันตัวตนไม่สำเร็จหรือมีข้อผิดพลาด
                {
                    statuscard = false;
                    PrintNoDataRfidfail();
                }

            }
        }
        //สำหรับคนที่บัตรหมดอายุ
        public void oracleCheck(string StdID)
        {
            String OracleConnetionString = "Data Source = (DESCRIPTION = " +
                "(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.100.177)(PORT = 1521))" +
                "(CONNECT_DATA = " +
                "(SERVER = DEDICATED)" +
                "(SERVICE_NAME = alist4)" +
                ")" +
                ");User Id = report;password=report123;";
            string pushExData = " ";
            string exfirstname = " ";
            string exlastname = " ";
            string extitleid = " ";
            string exmember = " ";
            string exfaculty = " ";
            string exdepartment = " ";

            OracleConnection con = new OracleConnection(OracleConnetionString);
            try
            {
                con.Open();
                OracleCommand cmd = new OracleCommand("SELECT * FROM REPORT.CIRMEMBER WHERE MEMBERCODE= '" + StdID + "'", con);
                OracleDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.Read())
                {
                    string dateNow = DateTime.Now.ToString(); //วันปัจจุบัน
                    string expireDate = dataReader[8].ToString(); //วันหมดอายุในบัตร

                    //เช็ควันหมดอายุของบัตร
                    //ถ้าหมดอายุ
                    if (DateTime.Parse(expireDate) < DateTime.Parse(dateNow))
                    {
                        statuscard = false;
                        UpdatingTxtAll2("   ชื่อ-สกุล            :   " + dataReader[1].ToString() + "   " + dataReader[2].ToString());
                        UpdatingTxtAll3("   วันที่เข้าใช้งาน  :   บัตรหมดอายุ ");
                        UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   บัตรหมดอายุ ");
                        Image imageCheck2 = null;
                        imageCheck2 = Image.FromFile(@"\software-and-hardware\picture\expired.jpg");    //แสดงรูปภาพ Nodata
                        pictureBox3.Image = imageCheck2;
                        pictureBox2.Image = imageCheck2;
                        PlaySound3();
                        Loggerexpired("stdID: " + StdID + "   FirstName: " + dataReader[1].ToString() + "     SureName: " + dataReader[2].ToString() + "        Status: false");
                    }
                    else
                    {
                        exfirstname = dataReader[1].ToString();
                        exlastname = dataReader[2].ToString();
                        extitleid = dataReader[3].ToString();
                        exmember = dataReader[4].ToString();
                        exfaculty = dataReader[5].ToString();
                        exdepartment = dataReader[6].ToString();

                        statuscard = true;
                        UpdatingTxtAll2("   ชื่อ-สกุล            :   " + dataReader[1].ToString() + "   " + dataReader[2].ToString());
                        UpdatingTxtAll3("   วันที่เข้าใช้งาน   :   " + DateTime.Now.ToString("dd/MM/yyyy"));
                        UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   " + DateTime.Now.ToString("HH:mm:ss น."));
                        //logCard รหัส ชื่อ สกุล วันเวลา
                        LoggerCard("stdID: " + StdID + "   FirstName: " + dataReader[1].ToString() + "     SureName: " + dataReader[2].ToString());
                        Console.WriteLine("################" + StdID);

                        //ถ้ามีรหัส น.ศ ฐานข้อมูลจะทำการดึงรูปภาพจากฐานข้อมูลมาแสดง
                        /*
                        Image imageCheck1 = null;
                        imageCheck1 = Image.FromFile(@"\software-and-hardware\picture\" + StdID + ".jpg");
                         pictureBox3.Image = imageCheck1;
                         */

                        Picweb(StdID);

                        //ถ่ายภาพ
                        useCapture();
                        //เล่นเสียงยินดีย้อนรับค่ะ
                        PlaySound1();
                        //ไฟเขียวติด 4 วินาที
                        lightAsync();
                        pushExData = "OK";
                    }
                }
                dataReader.Close();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception) //ทำงานเมื่อ เกิด Exception
            {
                statuscard = false;
                PrintNoDataRfidfail();
            }
            if (pushExData == "OK")
            {
                BarcodeandRFIDCMD(StdID, exfirstname, exlastname, extitleid, exmember, exfaculty, exdepartment);
            }
        }

        //ฟังชัน์ที่นำไปเรียกใช้ ใน barcode qrcode และ rfid
        public void BarcodeandRFIDCMD(string StdID, string firstname, string lastname, string titleid, string member, string faculty, string department)
        {
            NpgsqlConnection connection = new NpgsqlConnection(connstring);
            try
            {
                DateTime timetora = DateTime.Now;
                timetora = timetora.AddTicks(-(timetora.Ticks % TimeSpan.TicksPerSecond));

                TimeSpan timeNow = DateTime.Now.TimeOfDay;
                TimeSpan trimmedTimeNow = new TimeSpan(timeNow.Hours, timeNow.Minutes, timeNow.Seconds);

                int weektime = (int)DateTime.Today.DayOfWeek;
                string major = "0";

                connection.Open();
                NpgsqlCommand command2 = new NpgsqlCommand("INSERT INTO door_log (pid, fname, lname, titleid, membertype, facultyid, departmentid, majorid, timein, datein, weekday) VALUES(:sID, :fname, :lname, :title, :membertype, :facultyid, :departmentid, :majorid, :time, :date, :week)", connection);
                command2.Parameters.Add(new NpgsqlParameter("sid", StdID));
                command2.Parameters.Add(new NpgsqlParameter("fname", firstname));
                command2.Parameters.Add(new NpgsqlParameter("lname", lastname));
                command2.Parameters.Add(new NpgsqlParameter("title", titleid));
                command2.Parameters.Add(new NpgsqlParameter("membertype", member));
                command2.Parameters.Add(new NpgsqlParameter("facultyid", faculty));
                command2.Parameters.Add(new NpgsqlParameter("departmentid", department));
                command2.Parameters.Add(new NpgsqlParameter("majorid", major));
                command2.Parameters.Add(new NpgsqlParameter("time", trimmedTimeNow));
                command2.Parameters.Add(new NpgsqlParameter("date", timetora));
                command2.Parameters.Add(new NpgsqlParameter("week", weektime.ToString()));
                command2.ExecuteNonQuery();
                command2.Dispose();
                connection.Close();
            }
            catch (Exception) //ทำงานเมื่อ เกิด Exception
            {
                Console.WriteLine(">>>>>>ERROR push");
            }
        }
        public void QRCMD(string barcodeId, string typeUser)
        {
            NpgsqlConnection connection = new NpgsqlConnection(connstring);
            try
            {
                DateTime timetora = DateTime.Now;
                timetora = timetora.AddTicks(-(timetora.Ticks % TimeSpan.TicksPerSecond));

                TimeSpan timeNow = DateTime.Now.TimeOfDay;
                TimeSpan trimmedTimeNow = new TimeSpan(timeNow.Hours, timeNow.Minutes, timeNow.Seconds);

                int weektime = (int)DateTime.Today.DayOfWeek;

                string Barstr = barcodeId;
                string titleid = Barstr.Substring(4, 4);


                //เข้าฐานข้อมูล
                connection.Open();
                NpgsqlCommand command2 = new NpgsqlCommand("INSERT INTO door_log (pid, fname, lname, titleid, membertype, facultyid, departmentid, majorid, timein, datein, weekday) VALUES(:BID, 'exterior person', :lname, :title, 'coupon', 'coupon', 'EX', 'EX', :time, :date, :week)", connection);
                command2.Parameters.Add(new NpgsqlParameter("BID", barcodeId));
                command2.Parameters.Add(new NpgsqlParameter("lname", typeUser));
                command2.Parameters.Add(new NpgsqlParameter("title", titleid));
                command2.Parameters.Add(new NpgsqlParameter("time", trimmedTimeNow));
                command2.Parameters.Add(new NpgsqlParameter("date", timetora));
                command2.Parameters.Add(new NpgsqlParameter("week", weektime.ToString()));

                command2.ExecuteNonQuery();
                command2.Dispose();
                connection.Close();
            }
            catch (Exception) //ทำงานเมื่อ เกิด Exception
            {
                Console.WriteLine(">>>>>>ERROR push");
            }
        }
        public void Picweb(string StdID)
        {
            try
            {
                string studentId = StdID;
                string categoryId = "";
                string result = obj.GetFileByStudent(studentId, categoryId);
                string checkHttps = "https";
                int checkCharacter = 0;
                while ((checkCharacter = result.IndexOf(checkHttps, checkCharacter)) != -1)
                {
                    result = result.Substring(checkCharacter);
                    checkCharacter++;
                }
                string cuthttps = result;
                string cutid = Reverse(cuthttps);
                string checkid = Reverse(studentId);
                checkCharacter = 0;
                while ((checkCharacter = cutid.IndexOf(checkid, checkCharacter)) != -1)
                {
                    cutid = cutid.Substring(checkCharacter);
                    checkCharacter++;
                }
                string cutjpg = cutid;
                string checkjpg = "GPJ.";
                checkCharacter = 0;
                while ((checkCharacter = cutjpg.IndexOf(checkjpg, checkCharacter)) != -1)
                {
                    cutjpg = cutjpg.Substring(checkCharacter);
                    checkCharacter++;
                }
                string urlPicture = Reverse(cutjpg);
                Console.WriteLine("url Picture = " + urlPicture);
                WebRequest request = WebRequest.Create(urlPicture);
                using (var response = request.GetResponse())
                {
                    using (var showPictureUrl = response.GetResponseStream())
                    {
                        pictureBox3.Image = Image.FromStream(showPictureUrl);
                    }
                }
            }

            catch (Exception) //ทำงานเมื่อ เกิด Exception
            {
                try
                {
                    string staff = ("https://dss.psu.ac.th/dss_person/images/staff/" + StdID.ToString() + ".jpg");
                    Console.WriteLine("url staff Picture = " + staff);
                    WebRequest request = WebRequest.Create(staff);
                    using (var response = request.GetResponse())
                    {
                        using (var showPictureUrl = response.GetResponseStream())
                        {
                            pictureBox3.Image = Image.FromStream(showPictureUrl);
                        }
                    }

                }
                catch (Exception) //ทำงานเมื่อ เกิด Exception
                {
                    Image imageCheck2 = null;
                    imageCheck2 = Image.FromFile(@"\software-and-hardware\picture\nopic.jpg");
                    pictureBox3.Image = imageCheck2;
                }
            }
        }

        //การทำงานของไฟเขียวและ แดง
        public async void lightAsync()
        {
            timeCheck = 10;
            serialPort1.RtsEnable = true; //ไฟเขียวติด
            do
            {
                Console.WriteLine(">>>>>>>>" + timeCheck);
                UpdatingtxtCountDown(timeCheck.ToString());
                timeCheck--;
                await Task.Delay(1000);
                if (timeCheck == 0)
                {
                    serialPort1.RtsEnable = false;
                    timeCheck = 10;
                    UpdatingtxtCountDown(timeCheck.ToString());
                    statusbarcode = false;
                    statuscard = false;
                }

            } while (serialPort1.RtsEnable == true);
        }

        //กลับค่าstring
        public string Reverse(string str)
        {
            char[] array = str.ToCharArray();
            Array.Reverse(array);
            return new string(array);
        }

        //log ไฟล์แสดงประวัติการเข้าใช้งาน
        public static void VerifyDir(string path)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                if (!dir.Exists)
                {
                    dir.Create();
                }
            }
            catch { }
        }
        public static void LoggerCard(string lines)
        {
            string path = @"\software-and-hardware\LogCards\";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + " " + DateTime.Now.Month.ToString() + " " + DateTime.Now.Year.ToString() + "_Cards.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine(DateTime.Now.ToString() + ": " + lines);
                file.Close();
            }
            catch (Exception) { }
        }
        public static void LoggerBarcode(string lines)
        {
            string path = @"\software-and-hardware\LogBarcode\";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + " " + DateTime.Now.Month.ToString() + " " + DateTime.Now.Year.ToString() + "_Barcode.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine(DateTime.Now.ToString() + ": " + lines);
                file.Close();
            }
            catch (Exception) { }
        }
        public static void LoggerCount(string lines)
        {
            string path = @"\software-and-hardware\LogCount\";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + " " + DateTime.Now.Month.ToString() + " " + DateTime.Now.Year.ToString() + "_Count.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine(DateTime.Now.ToString() + ": " + lines);
                file.Close();
            }
            catch (Exception) { }
        }
        public static void Loggerexpired(string lines)
        {
            string path = @"\software-and-hardware\LogExpired\";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + " " + DateTime.Now.Month.ToString() + " " + DateTime.Now.Year.ToString() + "_Expired.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine(DateTime.Now.ToString() + ": " + lines);
                file.Close();
            }
            catch (Exception) { }
        }

        //ฟังชัน์ที่นำไปเรียกใช้ เมื่อไม่มีข้อมูล จากการอ่าน rfid และ barcode
        private void PrintNoDataRfid()
        {
            //แสดงว่า ไม่พบข้อมูล
            UpdatingTxtAll2("   ชื่อ-สกุล            :   ไม่พบข้อมูล");
            UpdatingTxtAll3("   วันที่เข้าใช้งาน  :   ไม่พบข้อมูล ");
            UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   ไม่พบข้อมูล ");
            Image imageCheck2 = null;
            imageCheck2 = Image.FromFile(@"\software-and-hardware\picture\nodata.jpg");    //แสดงรูปภาพ Nodata
            pictureBox3.Image = imageCheck2;
            pictureBox2.Image = imageCheck2;
            PlaySound2();     //เล่นเสียงไม่พบข้อมูลค่ะ
        }
        private void PrintNoDataRfidfail()
        {
            //แสดงว่า ไม่พบข้อมูล
            UpdatingTxtAll2("   ชื่อ-สกุล            :   ไม่พบข้อมูล");
            UpdatingTxtAll3("   วันที่เข้าใช้งาน  :   ไม่พบข้อมูล ");
            UpdatingTxtAll4("   เวลาที่เข้าใช้งาน :   ไม่พบข้อมูล ");
            Image imageCheck2 = null;
            imageCheck2 = Image.FromFile(@"\software-and-hardware\picture\nodata.jpg");    //แสดงรูปภาพ Nodata
            pictureBox3.Image = imageCheck2;
            pictureBox2.Image = imageCheck2;
            PlaySound6();     //เล่นเสียงไม่พบข้อมูลค่ะ
        }
        private void PrintNoDataBarcode()
        {
            //แสดงว่า ไม่พบข้อมูล
            UpdatingTxtAll2("   ประเภทบุคคล      :   ไม่พบข้อมูล");
            UpdatingTxtAll3("   วันที่เข้าใช้งาน      :   ไม่พบข้อมูล ");
            UpdatingTxtAll4("   เวลาที่เข้าใช้งาน     :   ไม่พบข้อมูล ");
            Image imageCheck2 = null;
            imageCheck2 = Image.FromFile(@"\software-and-hardware\picture\nodata.jpg");    //แสดงรูปภาพ Nodata
            pictureBox3.Image = imageCheck2;
            pictureBox2.Image = imageCheck2;
            PlaySound2();     //เล่นเสียงไม่พบข้อมูลค่ะ
        }

        //การทำงานของการแคปภาพจากกล้อง
        private void useCapture()
        {
            if (isCameraRunning)
            {
                // Take snapshot of the current image generate by OpenCV in the Picture Box
                Image exportCapture = null;
                exportCapture = pictureBox1.Image;
                //Bitmap snapshot = new Bitmap(exportCapture);
                string name_pic = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                // Save in some directory
                exportCapture.Save(string.Format(@"\software-and-hardware\capture\" + name_pic + ".jpg", Guid.NewGuid()), ImageFormat.Jpeg);
                Image imageTest = null;
                imageTest = Image.FromFile(@"\software-and-hardware\capture\" + name_pic + ".jpg");
                pictureBox2.Image = imageTest;
            }
            else
            {
                Console.WriteLine("Cannot take picture if the camera isn't capturing image!");
                Image imageCheckCamera = null;
                imageCheckCamera = Image.FromFile(@"\software-and-hardware\picture\nodata.jpg");
                pictureBox1.Image = imageCheckCamera;
                pictureBox2.Image = imageCheckCamera;
                isCameraRunning = false;
            }
        }

       //ตั้งค่าเวลา
        private void StartTimer()
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(tmr_Tick);
            tmr.Enabled = true;
        }
        //แสดงเวลาลงใน textbox
        void tmr_Tick(object sender, EventArgs e)
        {
            textBox2.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }

        //เสียงยินดีต้อนรับค่ะ
        private void PlaySound1()
        {
            SoundPlayer player1 = new SoundPlayer();
            player1.SoundLocation = @"\software-and-hardware\sound\ยินดีต้อนรับค่ะ.wav";
            player1.Play();
        }
        //เสียงไม่พบข้อมูลค่ะ
        private void PlaySound2()
        {
            SoundPlayer player2 = new SoundPlayer();
            player2.SoundLocation = @"\software-and-hardware\sound\ไม่พบข้อมูลค่ะ.wav";
            player2.Play();
        }
        //เสียงบัตรของคุณหมดอายุแล้วค่ะ
        private void PlaySound3()
        {
            SoundPlayer player3 = new SoundPlayer();
            player3.SoundLocation = @"\software-and-hardware\sound\บัตรของคุณหมดอายุแล้วค่ะ.wav";
            player3.Play();
        }
        //เสียงคูปองสำหรับเด็ก
        private void PlaySound4()
        {
            SoundPlayer player4 = new SoundPlayer();
            player4.SoundLocation = @"\software-and-hardware\sound\คูปองสำหรับเด็ก.wav";
            player4.Play();
        }
        //เสียงคูปองสำหรับผู้ใหญ่
        private void PlaySound5()
        {
            SoundPlayer player5 = new SoundPlayer();
            player5.SoundLocation = @"\software-and-hardware\sound\คูปองสำหรับผู้ใหญ่.wav";
            player5.Play();
        }
        //เสียงสแกนอีกครั้ง
        private void PlaySound6()
        {
            SoundPlayer player6 = new SoundPlayer();
            player6.SoundLocation = @"\software-and-hardware\sound\สแกนอีกครั้ง.wav";
            player6.Play();
        }
        //เสียงสแกนบัตรด้วย
        private void PlaySound7()
        {
            SoundPlayer player7 = new SoundPlayer();
            player7.SoundLocation = @"\software-and-hardware\sound\สแกนบัตรด้วย.wav";
            player7.Play();
        }
        //เสียงqrcode หมดอายุ
        private void PlaySound8()
        {
            SoundPlayer player8 = new SoundPlayer();
            player8.SoundLocation = @"\software-and-hardware\sound\คิวอาร์โค้ดของคุณหมดอายุแล้วค่ะ.wav";
            player8.Play();
        }


        //ฟังก์ชั่นตรวจสอบบัตรแต่ล่ะ Block 
        public string verifyCard(String Block)
        {
            string value = "";
            if (connectCard())
            {
                value = readBlock(Block);
            }
            
            value = value.Split(new char[] { '\0' }, 2, StringSplitOptions.None)[0].ToString();
            return value;
        }
        //อ่านค่าที่อยู่ใน Block แต่ล่ะ Block ของบัตร
        public string readBlock(String Block)
        {
            string tmpStr = "";
            int indx;

            if (authenticateBlock(Block))
            {
                ClearBuffers();
                SendBuff[0] = 0xFF; // CLA 
                SendBuff[1] = 0xB0;// INS
                SendBuff[2] = 0x00;// P1
                SendBuff[3] = (byte)int.Parse(Block);// P2 : Block No.
                SendBuff[4] = (byte)int.Parse("16");// Le

                SendLen = 5;
                RecvLen = SendBuff[4] + 2;

                retCode = SendAPDUandDisplay(2);

                if (retCode == -200)
                {
                    return "outofrangeexception";
                }

                if (retCode == -202)
                {
                    return "BytesNotAcceptable";
                }

                if (retCode != Card.SCARD_S_SUCCESS)
                {
                    return "FailRead";
                }

                // Display data in text format
                for (indx = 0; indx <= RecvLen - 1; indx++)
                {
                    tmpStr = tmpStr + Convert.ToChar(RecvBuff[indx]);
                }

                return (tmpStr);
            }
            else
            {
               return "FailAuthentication";
            }
        }
        // block authentication
        private bool authenticateBlock(String block)
        {
            ClearBuffers();
            SendBuff[0] = 0xFF;                         // CLA
            SendBuff[2] = 0x00;                         // P1: same for all source types 
            SendBuff[1] = 0x86;                         // INS: for stored key input
            SendBuff[3] = 0x00;                         // P2 : Memory location;  P2: for stored key input
            SendBuff[4] = 0x05;                         // P3: for stored key input
            SendBuff[5] = 0x01;                         // Byte 1: version number
            SendBuff[6] = 0x00;                         // Byte 2
            SendBuff[7] = (byte)int.Parse(block);       // Byte 3: sectore no. for stored key input
            SendBuff[8] = 0x60;                         // Byte 4 : Key A for stored key input
            SendBuff[9] = (byte)int.Parse("1");         // Byte 5 : Session key for non-volatile memory

            SendLen = 0x0A;
            RecvLen = 0x02;

            retCode = SendAPDUandDisplay(0);

            if (retCode != Card.SCARD_S_SUCCESS)
            {
                //MessageBox.Show("FAIL Authentication!");
                return false;
            }

            return true;
        }
        // clear memory buffers
        private void ClearBuffers()
        {
            long indx;

            for (indx = 0; indx <= 262; indx++)
            {
                RecvBuff[indx] = 0;
                SendBuff[indx] = 0;
            }
        }
        public string Transmit(byte[] buff)
        {
            string tmpStr = "";
            int indx;

            ClearBuffers();

            for (int i = 0; i < buff.Length; i++)
            {
                SendBuff[i] = buff[i];
            }
            SendLen = 5;
            RecvLen = SendBuff[SendBuff.Length - 1] + 2;

            retCode = SendAPDUandDisplay(2);


            // Display data in text format
            for (indx = 0; indx <= RecvLen - 1; indx++)
            {
                tmpStr = tmpStr + Convert.ToChar(RecvBuff[indx]);
            }

            return tmpStr;
        }
        // send application protocol data unit : communication unit between a smart card reader and a smart card
        private int SendAPDUandDisplay(int reqType)
        {
            int indx;
            string tmpStr = "";

            pioSendRequest.dwProtocol = Aprotocol;
            pioSendRequest.cbPciLength = 8;

            //Display Apdu In
            for (indx = 0; indx <= SendLen - 1; indx++)
            {
                tmpStr = tmpStr + " " + string.Format("{0:X2}", SendBuff[indx]);
            }

            retCode = Card.SCardTransmit(hCard, ref pioSendRequest, ref SendBuff[0],
                                 SendLen, ref pioSendRequest, ref RecvBuff[0], ref RecvLen);

            if (retCode != Card.SCARD_S_SUCCESS)
            {
                return retCode;
            }

            else
            {
                try
                {
                    tmpStr = "";
                    switch (reqType)
                    {
                        case 0:
                            for (indx = (RecvLen - 2); indx <= (RecvLen - 1); indx++)
                            {
                                tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);
                            }

                            if ((tmpStr).Trim() != "90 00")
                            {
                                //MessageBox.Show("Return bytes are not acceptable.");
                                return -202;
                            }

                            break;

                        case 1:

                            for (indx = (RecvLen - 2); indx <= (RecvLen - 1); indx++)
                            {
                                tmpStr = tmpStr + string.Format("{0:X2}", RecvBuff[indx]);
                            }

                            if (tmpStr.Trim() != "90 00")
                            {
                                tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);
                            }

                            else
                            {
                                tmpStr = "ATR : ";
                                for (indx = 0; indx <= (RecvLen - 3); indx++)
                                {
                                    tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);
                                }
                            }

                            break;

                        case 2:

                            for (indx = 0; indx <= (RecvLen - 1); indx++)
                            {
                                tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);
                            }

                            break;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    return -200;
                }
            }
            return retCode;
        }

        //เชื่อต่อกับอุปกรณ์
        public void SelectDevice()
        {
            List<string> availableReaders = this.ListReaders();
            this.RdrState = new Card.SCARD_READERSTATE();
            readername = availableReaders[0].ToString();//selecting first device
            this.RdrState.RdrName = readername;
        }
        public List<string> ListReaders()
        {
            int ReaderCount = 0;
            List<string> AvailableReaderList = new List<string>();

            //Make sure a context has been established before 
            //retrieving the list of smartcard readers.
            retCode = Card.SCardListReaders(hContext, null, null, ref ReaderCount);
            if (retCode != Card.SCARD_S_SUCCESS)
            {
                MessageBox.Show(Card.GetScardErrMsg(retCode));
                //connActive = false;
            }

            byte[] ReadersList = new byte[ReaderCount];

            //Get the list of reader present again but this time add sReaderGroup, retData as 2rd & 3rd parameter respectively.
            retCode = Card.SCardListReaders(hContext, null, ReadersList, ref ReaderCount);
            if (retCode != Card.SCARD_S_SUCCESS)
            {
                MessageBox.Show(Card.GetScardErrMsg(retCode));
            }

            string rName = "";
            int indx = 0;
            if (ReaderCount > 0)
            {
                // Convert reader buffer to string
                while (ReadersList[indx] != 0)
                {

                    while (ReadersList[indx] != 0)
                    {
                        rName = rName + (char)ReadersList[indx];
                        indx = indx + 1;
                    }

                    //Add reader name to list
                    AvailableReaderList.Add(rName);
                    rName = "";
                    indx = indx + 1;

                }
            }
            return AvailableReaderList;
        }
        //สร้างการเชื่อต่อ
        internal void establishContext()
        {
            retCode = Card.SCardEstablishContext(Card.SCARD_SCOPE_SYSTEM, 0, 0, ref hContext);
            if (retCode != Card.SCARD_S_SUCCESS)
            {
               
                connActive = false;
                return;
            }
        }
        //ฟังก์ชั่นไม่เชื่อต่อกับบัตร
        public void Disconnect()
        {
            if (connActive)
            {
                retCode = Card.SCardDisconnect(hCard, Card.SCARD_UNPOWER_CARD);
                //เมื่อไม่มีการเชื่อต่อจะทำการอัพเดท textbox ให้ว่างเปล่า
            }
            // retCode = Card.SCardReleaseContext(hCard);
        }
        //คืนค่าเป็น true เมื่อ เชื่อมบัตร false เมื่อไม่ได้เชื่อม
        public bool connectCard()
        {
            connActive = true;

            retCode = Card.SCardConnect(hContext, readername, Card.SCARD_SHARE_SHARED,
                      Card.SCARD_PROTOCOL_T0 | Card.SCARD_PROTOCOL_T1, ref hCard, ref Protocol);

            if (retCode != Card.SCARD_S_SUCCESS)
            {

                connActive = false;
                return false;
            }
            return true;
        }
        //ฟังก์ชั่นการทำงานทำงานแบบ backgroundworker
        public void Watch()
        {
            this.RdrState = new Card.SCARD_READERSTATE();
            this.RdrState.RdrName = readername;

            states = new Card.SCARD_READERSTATE[1];
            states[0] = new Card.SCARD_READERSTATE();
            states[0].RdrName = readername;
            states[0].UserData = 0;
            states[0].RdrCurrState = Card.SCARD_STATE_EMPTY;
            states[0].RdrEventState = 0;
            states[0].ATRLength = 0;
            states[0].ATRValue = null;
            this._worker = new BackgroundWorker();
            this._worker.WorkerSupportsCancellation = true;

            this._worker.DoWork += WaitChangeStatus;

            this._worker.RunWorkerAsync();
        }

        //delegate เพื่อแสดงข้อความใน textbox
        delegate void TextBoxDelegate(string message);
        //textbox แสดง วัน
        public void UpdatingTxtAll4(string msg)
        {
            if (this.txtDisplayDate.InvokeRequired)
                this.txtDisplayDate.Invoke(new TextBoxDelegate(UpdatingTxtAll4), new object[] { msg });

            else
                this.txtDisplayDate.Text = msg;
        }
        //textbox แสดง สกุล
        public void UpdatingTxtAll3(string msg)
        {
            if (this.txtDisplaySurname.InvokeRequired)
                this.txtDisplaySurname.Invoke(new TextBoxDelegate(UpdatingTxtAll3), new object[] { msg });

            else
                this.txtDisplaySurname.Text = msg;
        }
        //textbox แสดง ชื่อ
        public void UpdatingTxtAll2(string msg)
        {
            if (this.txtDisplayFirstName.InvokeRequired)
                this.txtDisplayFirstName.Invoke(new TextBoxDelegate(UpdatingTxtAll2), new object[] { msg });

            else
                this.txtDisplayFirstName.Text = msg;
        }
        //textbox แสดง รหัสนักศึกษา
        public void UpdatingTxtAll(string msg)
        {
            if (this.txtDisplayStdID.InvokeRequired)
                this.txtDisplayStdID.Invoke(new TextBoxDelegate(UpdatingTxtAll), new object[] { msg });

            else
                this.txtDisplayStdID.Text = msg;
        }

        private void txtBoxAll3_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtBoxAll2_TextChanged(object sender, EventArgs e)
        {

        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
        private void txtSurename_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtBoxAll1_TextChanged(object sender, EventArgs e)
        {

        }    
        private void txtDate_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtDisplayDate_TextChanged(object sender, EventArgs e)
        {

        }
        private void tbDataItems_TextChanged(object sender, EventArgs e)
        {

        }
        private void Label10_Click(object sender, EventArgs e)
        {

        }
        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PictureBox5_Click(object sender, EventArgs e)
        {
            isCameraRunning = true;
            webcam = new WebCam();
            webcam.InitializeWebCam(ref pictureBox1);
            webcam.Start();
        }

        private void txtUid_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtFirstName_TextChanged(object sender, EventArgs e)
        {

        }
        private void label5_Click(object sender, EventArgs e)
        {

        }
        private void label7_Click(object sender, EventArgs e)
        {

        }
        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
